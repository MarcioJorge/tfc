<?php

    $erro= isset($_Get['erro']) ? $_GET['erro']:0;


?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Portal Empresarial</title>
    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="estiloind.css" rel="stylesheet">

    <!--cenas do icon do rodape -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <nav class="navbar navbar-fixed-top navbar-inverse navbar-transparente">

        <div class="container">

            <!-- header -->
            <div class="navbar-header">

                <!-- botao toggle -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#barra-navegacao">
                    <span class="sr-only">alternar navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="index.php" class="navbar-brand">
                    <span class="img-logo"></span>
                </a>

            </div>

            <!-- navbar -->
            <div class="collapse navbar-collapse" id="barra-navegacao">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="">Noticias</a></li>
                    <li><a href="">Perguntas</a></li>
                    <li class="divisor" role="separator"></li>
                    <li><a href="sobrenos.html">Sobre nós</a></li>
                    <li><a href="contactos.html">contactos</a></li>
                </ul>
            </div>
        </div><!-- /container -->
    </nav><!-- /nav -->


    <div class="capa">
        <div class="texto-capa">
            <h1>Portal Empresarial</h1>

     
            <button type="button" class="btn btn-custom btn-roxo btn-lg" data-toggle="modal" data-target="#janela">
                Login
            </button>

            <a href="registro.php" class="btn btn-custom btn-branco btn-lg">Cadastre-se</a>
        </div>
    </div>


    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <!-- Janela -->
    <form class="modal fade" method="POST" action="validar_acesso.php" id="janela" style="margin-top: 100px">

        <div class="modal-dialog modal-md" method="POST" action="validar_acesso.php">
            <div class="modal-content">
                
                <!-- cabecalho -->
                <div class="modal-header" style="color: white; background: #343444">
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                    <h4 class="modal-title">Efetuar login</h4>
                </div>

                <!-- corpo -->
                <div class="modal-body" id="janela">
  
                <form method="POST" action="validar_acesso.php" id="Formulario_de_Registro">
                    <div class="form-group">
                        <input type="email" class="form-control" id="email_usuario" name="email" placeholder="Digite seu e-mail" required="requiored">
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="senha_usuario" name="senha" placeholder="digite sua senha" required="requiored">
                    </div>

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="dropdownCheck2">
                        <label class="form-check-label" for="dropdownCheck2">
                            Lembra-me
                        </label>
                    </div>
                    <button type="submit"  class="btn botaoUso form-control">
                        Login
                    </button>
                    <a type="text" style="padding:20px;">Esqueci-me da passeword </a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancelar
                    </button>
                 
                </form>
                <?php
                    if($erro == 1){
                        echo 'Usuarios E OU senha invalodo(s)';
                    }
                ?>
                   
                </div>


            </div>
        </div>

    </form>

    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <!-- Conteudos -->
    <section id="servicos">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <h2> Empresas </h2>
                    <p style="text-align: justify;">

                        Porque é que o usamos?
                        É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando
                        analisa a sua mancha gráfica. Logo, o uso de Lorem Ipsum leva a uma distribuição mais ou menos
                        normal de letras, ao contrário do uso de "Conteúdo aqui, conteúdo aqui", tornando-o texto
                        legível. Muitas ferramentas de publicação electrónica e editores de páginas web usam actualmente
                        o Lorem Ipsum como o modelo de texto usado por omissão, e uma pesquisa por "lorem ipsum" irá
                        encontrar muitos websites ainda na sua infância. Várias versões têm evoluído ao longo dos anos,
                        por vezes por acidente, por vezes propositadamente (como no caso do humor).
                    </p>

                </div>

                <!-- servicos -->
                <div class="col-md-6">
                    <h2> Empreendedores </h2>
                    <p style="text-align: justify;">
                        Porque é que o usamos?
                        É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando
                        analisa a sua mancha gráfica. Logo, o uso de Lorem Ipsum leva a uma distribuição mais ou menos
                        normal de letras, ao contrário do uso de "Conteúdo aqui, conteúdo aqui", tornando-o texto
                        legível. Muitas ferramentas de publicação electrónica e editores de páginas web usam actualmente
                        o Lorem Ipsum como o modelo de texto usado por omissão, e uma pesquisa por "lorem ipsum" irá
                        encontrar muitos websites ainda na sua infância. Várias versões têm evoluído ao longo dos anos,
                        por vezes por acidente, por vezes propositadamente (como no caso do humor).
                    </p>

                </div>
            </div>
        </div>

    </section>

    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <section id="recursos">
        <div class="container">
            <div class="row">

                <!-- recursos -->
                <div class="col-md-6">
                    <img src="imagens/Empresas-3.png" class="redimg img-responsive">

                </div>

                <!-- img recursos -->
                <div class="col-md-6">
                    <h2 style="color: #343444"> Investidores </h2>
                    <p style="text-align: justify; color: #343444">

                        Porque é que o usamos?
                        É um facto estabelecido de que um leitor é distraído pelo conteúdo legível de uma página quando
                        analisa a sua mancha gráfica. Logo, o uso de Lorem Ipsum leva a uma distribuição mais ou menos
                        normal de letras, ao contrário do uso de "Conteúdo aqui, conteúdo aqui", tornando-o texto
                        legível. Muitas ferramentas de publicação electrónica e editores de páginas web usam actualmente
                        o Lorem Ipsum como o modelo de texto usado por omissão, e uma pesquisa por "lorem ipsum" irá
                        encontrar muitos websites ainda na sua infância. Várias versões têm evoluído ao longo dos anos,
                        por vezes por acidente, por vezes propositadamente (como no caso do humor).
                    </p>

                </div>

            </div>
        </div>
    </section>
    
   
    <div class="barraDivisoria">
    </div>

    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <section id="Mais_info">
            
        <div class="container">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4" style="margin-left: 30px;">

                    <img src="imagens/logo_ulht.png" style="height: 200px; width: 200px; margin-top:30px;">

                </div>

                <div class="col-md-4" style="margin-left: 30px;">

                    <img src="imagens/ecati-700.jpg" style="height: 200px; width: 200px; margin-top:30px;">

                </div>
            
            </div>

        </div>
    </section>

    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <!-- Rodape -->
    <section id="Rodape">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <h5> &copy; 2019 Portal Empresarial <br> Todos os direiros Reservados</h5>
                    <a href="https://pt-pt.facebook.com" class="fa fa-facebook"></a>
                    <a href="https://twitter.com" class="fa fa-twitter"></a>
                    <a href="https://www.google.com" class="fa fa-google"></a>

                </div>
            </div>
        </div>
    </section>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>