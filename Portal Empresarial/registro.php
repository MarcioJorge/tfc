<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Registro</title>
    <link rel="icon" href="imagens/favicon.png">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="estilo.css" rel="stylesheet">
    <link href="estilo2.css" rel="stylesheet">

    <!--cenas do icon do rodape -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <nav class="navbar navbar-fixed-top navbar-inverse navbar-transparente">

        <div class="container">

            <!-- header -->
            <div class="navbar-header">

                <!-- botao toggle -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#barra-navegacao">
                    <span class="sr-only">alternar navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="index.html" class="navbar-brand">
                    <span class="img-logo"></span>
                </a>

            </div>

            <!-- navbar -->
            <div class="collapse navbar-collapse" id="barra-navegacao">
                <ul class="nav navbar-nav navbar-right">
                        <li><a href="feed.html">Noticias</a></li>
                        <li><a href="parcerias.html">Forum</a></li>
                        <li><a href="projeto.html">Projectos</a></li>
                        <li class="divisor" role="separator"></li>
    
                        <li>
                            <div class="btn-group">
                                <button type="button" class="btn dropdown-toggle botao-conta" 
                                        data-toggle="dropdown">
                                                Conta <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Perfil</a></li>
                                    <li><a href="#">Definições</a></li>
                                </ul>
                            </div>
    
                        </li>
                </ul>
            </div>
        </div><!-- /container -->
    </nav><!-- /nav -->



    <!--xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->
    <!-- Conteudos -->
    <section id="inscricao">
        <div class="container">
            <div class="row">
                <div class="col-sm-6" >
                    <h1>Cadastre-se</h1>
                    <form method="POST" action="registroUsuario.php" id="Formulario_de_Registro" >
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control" id="nome" name="nome" required="requiored">
                        </div>

                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email" name="email"
                                placeholder="portalempresatial@gmail.com" required="requiored">
                        </div>

                        <div class="form-group">
                            <label for="email">Repita o seu E-mail</label>
                            <input type="email" class="form-control" id="email2" name="email2"
                                placeholder="portalempresatial@gmail.com" required="requiored">
                        </div>

                        <div class="form-group">
                            <label for="senha">Password</label>
                            <input type="password" class="form-control" id="senha" name="senha" required="requiored">
                        </div>

                        <div class="form-group">
                            <label for="senha">Repita a sua Password</label>
                            <input type="password" class="form-control" id="senha2" name="senha2" required="requiored">
                        </div>

                        <div class="form-group">
                            <label for="Telemovel">Telemóvel</label>
                            <input type="text" class="form-control" maxlength="16" id="Telemovel" name="Telemovel" required="requiored">
                        </div>


                        <h5><b>Endereço:</b></h5>

                        <div class="form-row">
                            <div class="form-group col-md-3">
                                <label for="cidade">Cidade</label>
                                <input type="text" class="form-control" id="cidade" name="cidade" required="requiored">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="distrito">Distrito</label>
                                <select id="distrito" class="form-control" name="distrito" required="requiored">
                                    <option selected> </option>
                                    <option id="distrito">Lisboa</option>
                                    <option id="distrito">Porto</option>
                                    <option id="distrito">Braga</option>
                                </select>
                            </div>

                            <div class="form-group col-md-2">
                                <label for="codigoP">Zip</label>
                                <input type="text" class="form-control" id="codigoP" name="codigoP" required="requiored">
                            </div>

                            <div class="form-group col-md-3">
                                <label for="pais">Pais</label>
                                <input type="text" class="form-control" id="pais" name="pais" required="requiored">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Registrar como...</label>
                            <select class="form-control" id="TipoEnt" name="TipoEnt" required="requiored">
                                <option></option>
                                <option id="TipoEnt">Empresa</option>
                                <option id="TipoEnt">Investidor</option>
                                <option id="TipoEnt">Empreendedor</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="nif">NIF</label>
                            <input type="text" class="form-control" id="nif" name="nif" required="requiored">
                        </div>
                        <br>


                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Aceito os termos do serviço
                            </label>
                        </div>



                        <button type="submit" class="btn btn-default form-control">Cadastrar</button>

                    </form>
                </div>


                <div class="col-sm-1"></div>

                <div class="col-sm-5 espacamento">

                    <h1> Porque Nós? <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> </h1>

                    <div class="infoResg">
                        <h2>Seguro <span class="glyphicon glyphicon-lock" aria-hidden="true"></span></h2>

                        <h2>Rapido <span class="glyphicon glyphicon-indent-right" aria-hidden="true"></span></h2>

                        <h2>Simples <span class="glyphicon glyphicon-search" aria-hidden="true"></span></h2>

                        <h2> Melhores Oportunidades de Negócio<span class="glyphicon glyphicon-eur"
                                aria-hidden="true"></span></h2>

                    </div>

                </div>

            </div>

        </div>
    </section>

    <!-- Rodape -->
    <section id="Rodape">
        <div class="container">
            <div class="row">
                <div class="col-md-3">

                    <h5> &copy; 2019 Portal Empresarial <br> Todos os direiros Reservados</h5>
                    <a href="https://pt-pt.facebook.com" class="fa fa-facebook"></a>
                    <a href="https://twitter.com" class="fa fa-twitter"></a>
                    <a href="https://www.google.com" class="fa fa-google"></a>

                </div>
            </div>
        </div>
    </section>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>