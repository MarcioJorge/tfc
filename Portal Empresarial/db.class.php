<?php

class db{
    
    private $host ='localhost';

    private $usuario = 'root';

    private $senha='';

    private $database='portal_empresarial';

    

    public function conecta_mysql(){

        //criar a conecção
        $con = mysqli_connect($this->host, $this->usuario, $this->senha, $this->database);
        
        //ajustar o charset de comunicação entre a aplicacao e a database
        mysqli_set_charset($con, 'utf8');


        //verificar se houve erro
        if (mysqli_connect_errno()){

            echo 'Erro ao tentar se conectar com o BD MySQL'.mysqli_connect_error();
        }
        return $con;
    }
}
?>